## 西北农林科技大学学位论文LaTeX文档类(模板)
### **特别声明**
--------------------
本模板目前仅支持`XeTeX`和`LuaTeX`引擎，对其他引擎(包括pdfTeX和ApTeX)暂未列入支持计划。同时，本模板仅支持`UTF-8`编码的TeX源文件。

在您使用`nwafuthesis`之前，请务必仔细阅读模板文档[nwafuthesis.pdf](https://mirrors.aliyun.com/CTAN/macros/unicodetex/latex/nwafuthesis/nwafuthesis.pdf)

由于用到了一些LaTeX较新的特性及较新的宏包，该模板后期不再支持2021以下TeX发行版，请及时升级发行版并更新所有宏包。例如，TeXLive2023之后的发行版。

该模板的测试主要是在**TeXLive2023+Ubuntu 22.04**平台完成。

### 简介
---------------------

西北农林科技大学学位论文LaTeX文档类(模板)，支持本科(学士)、硕士(学硕/专硕)、博士学位论文。


#### 模板安装
--------------------
该模板已上传ctan，被TeX发行版收录，通过TeX发行版的包管理器即可完成该模板的安装。只需及时更新发行版，便可自动安装该模板并安装相关依赖。

作为终端用户，仅需要用`\documentclass{nwafuthesis}`载入该文档类(模板)即可使用该模板，其工作流如下图所示：

![](./screenshots/workingflow.png)

如果需要手动安装，则可执行相应的包管理命令。例如，对于TeXLive发行版，可在命令行执行如下命令进行安装(有可能需要管理员权限)：

```shell
tlmgr install nwafuthesis
```

当然，如果您对**模板开发**感兴趣，也可以进入`expl3-dtx`文件夹通过操作**nwafuthesis.dtx**是开发文档实现对模板的修改和完善，然后在命令行顺序执行如下操作:

```shell
  xelatex nwafuthesis.dtx
  makeindex -s gind.ist -o nwafuthesis.ind nwafuthesis.idx
  makeindex -s gglo.ist -o nwafuthesis.gls nwafuthesis.glo
  xelatex nwafuthesis.dtx
  xelatex nwafuthesis.dtx
  xelatex nwafuthesis.dtx
```
从而得到需要的`nwafuthesis.cls`模板文件和`nwafuthesis.pdf`说明文件。

#### 使用样例
--------------------
创建独立的工作目录，并在工作目录中创建`logo`文件夹，将学校的`nwafu-bar.pdf`校微矢量图(不可更改文件名)置于`logo`中，然后创建如下`main.tex`主文件(以本科学位论文为例)：
```tex
% 文档类(模板)
\documentclass[%
  type = bachelor,  % 本科论文(设计)
  % oneside,        % 单面模式
  twoside,          % 双面模式(openany)
]{nwafuthesis}
% 导言区

% 将需要载入的宏包统一在settings/package.tex中进行管理
\input{settings/packages.tex}
% 将必要的设置统一在settings/format.tex中进行管理
\input{settings/format.tex}

% 排版设置
\nwafuset{
% 论文格式
  style = {
    % font          = times*,           % 选择英文字体(需要安装有该字体，建议自动配置)
    % cjk-font      = adobe,            % 选择中文字体(需要安装有该字体，建议自动配置)
    hyperlink     = color,              % 超链接颜色
    % withchapter = false,              % 章标题是否为章模式
    % withsig = true,                   % 是否需要签名空位(仅研究生需要)
    % chapnum = zh,                     % 章编号方式(仅本科生需要,zh:中文数字，en:阿拉伯数字)
    bib-resource  = {bib/sample.bib},   % 参考文献数据源文件名，注意需要完整路径及后缀名
    % anonymous = true,                 % 是否输出盲审格式论文
  },
% 信息录入
  info = {
    grade = {2023},                                                % 毕业年份(届)
    enroll = {2019},                                               % 入学年份(级)
    class-id = {1},                                                % 班级号
    btype = {paper},                                               % 本科类型(论文/设计)
    % btype = {design},
    title = { \nwafuthesis{}快速上手示例文档 },                    % 中文题目
    title* = { \nwafuthesis{} Quick Start and Document Snippets }, % 英文题目
    department = { 信息工程学院 },                                 % 学院名称
    major = {计算机科学与技术},                                    % 专业
    author = { \TeX{}爱好者 },                                     % 作者中文名称
    supervisor = { 耿楠 },                                         % 指导教师
    cosupervisor = {Donald Knuth 大师},                            % 协助指导教师
    date = {\datezh},                                              % 论文提交时间
    student-id = {202301000},                                      % 学号
  },
}
% 录入摘要
\nwafuset{
  abstract = {
    abstractfile  = { contents/chap00-abs-zh.tex }, % 中文摘要文件名称，注意需要有.tex后缀名
    abstractfile* = { contents/chap00-abs-en.tex }, % 英文摘要文件名称，注意需要有.tex后缀名
    keywords      = {学位论文, 模板, \nwafuthesis}, % 中文关键字列表，注意用英文逗号分隔。
    keywords*     = {NWAFU thesis, document class, space is accepted here}, % 英文关键字列表，注意用英文逗号分隔
  },
}

% 正文区(有且只能有一个)
\begin{document}
% 这个命令用来关闭版心底部强制对齐，
% 可以减少不必要的 underfull \vbox 提示，但会影响排版效果
% \raggedbottom

% 由于论文模板中已设计了自动排版封面、摘要、目录等功能，
% 因此，无需手动排版。

% 主体部分是论文的核心
\mainmatter

% 建议采用多文件编译的方式编写论文，
% 比较好的做法是把每一章放进一个单独的 tex 文件里，
% 并在这里用 \include 导入，如：

% <论文主体>

% 打印参考文献列表
\bibmatter*
\printbibliography

% 排版附录，可选
\appendix
% <附录文档>

% 排版致谢
\backmatter
% <致谢文档>

% 个人简历， 本科生可选
% <个人简历文档>

\end{document}
```

更多样例，请分别参阅`demo`文件夹中`Bachelor`(本科)、`Master-Academic`(学硕)、`Master-Professional`(专硕)和`PhD`(博士)学位论文样例，可以直接通过修改样例完成学位论文(设计)排版。

由于模板已被收录于ctan，因此，论文并不依赖于当前工作路径中的`nwafuthesis.cls`文件，在论文撰写中，可以仅保留诸如`Bachelor`、`Master-Academic`、`Master-Professional`或`PhD`这样的工作目录即可。

排版样例源代码及模板开发源代码也可以通过[nwafuthesis发行版](https://gitee.com/nwafu_nan/nwafuthesis-l3/releases/)获得。

###  重要提醒
---------------------

1. 由于PDF格式文档与Word文档之间**不存在转换关系**，虽学校相关部门接收PDF格式的学位论文，但使用该模板之前请与导师/导师组进行充分沟通，以确认不需要进行PDF文档到Word文档的转换操作。
2. 本模板功能仍处于开发和完善中，不确保证接口的稳定性。在撰写论文的过程中，请慎重考虑是否要同步进行更新。
3. 任何由于使⽤本模板⽽引起的论⽂格式审查问题均与本模板作者⽆关。

###  致谢
---------------------

该模板参考了stone-zeng用LaTeX3开发的[复旦大学论文模板](https://github.com/stone-zeng/fduthesis)，通过添加/修改相关代码实现，另外，该模板开发过程中的参考文献样式由[biblatex-gb7714-2015样式包](https://github.com/hushidong/biblatex-gb7714-2015)作者胡振震开发的gb7714-NWAFU样式文件实现。在此，对他们表示由衷的感谢。

###  参与贡献
---------------------
1. 本项目由西北农林科技大学信息工程学院耿楠创建和维护
2. 如果您愿意一同参与工作(不计报酬，免费自由)，请及时与作者联系
3. 如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/nwafu_nan/nwafuthesis-l3/issues) 或 [pull request](https://gitee.com/nwafu_nan/nwafuthesis-l3/pulls)。
